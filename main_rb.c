#include <stdio.h>
#include <stdlib.h>
#include "line.h"
#include <iostream>
#include <string.h>
#include <vector>
#include <algorithm>
#include <time.h>
typedef struct line Insn;
typedef struct liveness Liveness;
typedef struct graph_node GNode;
typedef struct search_tree_node STNode;
map<int, GNode*>* cloneGraph(map<int, GNode*>* orig);
time_t start;
int edges;

int* parse_line(char * line)
{
  int count = 0;
  int * arr = (int *) malloc(20);
  int arr_count = 0;
  char c;
  char num[4];
  num[3] = '\0';
  int digit_count = 0;
  while((c = line[count]) != '\n')
  {
    if(c == ',')
    {
      if(digit_count != 0)
      {
        num[digit_count] = '\0';
        arr[arr_count] = atoi(num);
      }
      else
      {
        arr[arr_count] = -1;
      }
      digit_count = 0;
      arr_count++;
    }  
    else
    {
      num[digit_count] = c;
      digit_count++;
    }
    count++;
  }
  if(digit_count == 0)
  {
    arr[arr_count] = -1;
  }
  else
  {
    arr[arr_count] = atoi(num);
  }
  return arr;
}

int populate_instructions(char* filename, Insn** ret)
{
  FILE *fp = fopen(filename, "r");
  char line[20];
  fgets(line, 20, fp);
  int no_lines = atoi(line);

  Insn* _ret = (Insn*) calloc(no_lines, sizeof(Insn));  
  int i;
  for(i = 0; i < no_lines; i++)
  {
    _ret[i].alt_preds = vector<int>();
  }


  int count = 0;
  int* line_vals;
  while(fgets(line, 20, fp) != NULL)
  {
    line_vals = parse_line(line);
    _ret[count].def = line_vals[0];
    _ret[count].use[0] = line_vals[1];
    _ret[count].use[1] = line_vals[2];
    _ret[count].use[2] = line_vals[3];
    _ret[count].alt_dest = line_vals[4];
    if(line_vals[4] != -1)
    {
      _ret[line_vals[4]].alt_preds.push_back(count);
    }
    free(line_vals);
    count+=1;
  }
  fclose(fp);
  *ret = _ret;
  return no_lines;
}

void print_insns(Insn* insns, int count)
{
  int i;
  for(i = 0; i < count; i++)
  {
    if(insns[i].alt_dest == -1)
    {
      printf("%d=%d,%d,%d\n", insns[i].def, insns[i].use[0], insns[i].use[1], insns[i].use[2]);
    }
    else if(insns[i].alt_dest != -1 && (insns[i].use[0] != -1 || insns[i].use[1] != -1 || insns[i].use[2] != -1))
    {
      printf("if(%d,%d,%d) goto line %d\n", insns[i].use[0], insns[i].use[1], insns[i].use[2], insns[i].alt_dest);
    }
    else
    {
      printf("goto line %d\n", insns[i].alt_dest);
    }
  }
}

void printSet(set<int> int_set)
{
  set<int>::iterator it = int_set.begin();
  set<int>::iterator end = int_set.end();
  while(it != end)
  {
    printf("%d ", *it);
    it++;
  }
}

void addToIn(set<int>* in, set<int>* out,  Insn* start, int current_index)
{
  Insn* current = start +current_index;
  if(current->use[0] != -1)
  {
    in->insert(current->use[0]);
  }
  if(current->use[1] != -1)
  {
    in->insert(current->use[1]);
  }
  if(current->use[2] != -1)
  {
    in->insert(current->use[2]);
  }

  set<int> temp (*out);
  temp.erase(current->def);
  in->insert(temp.begin(), temp.end());

  
  //temp.~set();
}


void addToOut(set<int>* in, set<int>* out, Insn* start, int current, int length, Liveness* l)
{
  if(current+1 < length)
  {
    set<int> temp = (l + current + 1)->in;
    out->insert(temp.begin(), temp.end());
  }
  if((start + current)->alt_dest != -1)
  {
    set<int> temp = (l + (start+current)->alt_dest)->in;
    out->insert(temp.begin(), temp.end());
  }
}

Liveness* produceLiveness(Insn* insns, int length)
{
  Liveness * l = (Liveness *) calloc(length, sizeof(Liveness));
  int i;
  int init = 1;
  for(i = 0; i < length; i++)
  {
      l[i].in = set<int>();
      l[i].out = set<int>();
  }

  int count = 0;
  while(1)
  {
    int no_same = 0;
    printf("--------\n");
    for(i = length - 1; i >= 0; i--)
    {
      set<int> temp_in(l[i].in);
      set<int> temp_out(l[i].out);
      addToOut(&l[i].in, &l[i].out, insns, i, length, l);
      addToIn(&l[i].in, &l[i].out, insns, i);
      printSet(l[i].in);
      printf(",");
      printSet(l[i].out);
      printf("\n");
      if(temp_in == l[i].in && temp_out == l[i].out)
      {
        no_same++;
      }   
    }
    if(no_same == length)
    { 
      break;
    }
    count++;
  } 
  return l;
}

void freeInsns(Insn* insns, int lines)
{
  int i;
  for(i = 0; i < lines; i++) 
  {
    insns[i].alt_preds.~vector();
  }
  free(insns);
}

void freeLiveness(Liveness *l, int length)
{
  int i;
  for(i = 0; i < length; i++)
  {
    l[i].in.~set();
    l[i].out.~set();
  }
  free(l);
}

void printLiveness(Liveness *i, int length)
{
}

map<int, GNode*>* generateNodes(Liveness* live, int length)
{
  set<int> no_sets = set<int>();
  int i;
  map<int, GNode*>* graph = new map<int,GNode*>();
  printf("-------------\n");
  for(i = 0; i < length; i++)
  {
    no_sets.insert(live[i].in.begin(), live[i].in.end());
    no_sets.insert(live[i].out.begin(), live[i].out.end());
    int j;
    set<int>::iterator in_it = live[i].in.begin();
    set<int>::iterator in_back = live[i].in.begin();
    set<int>::iterator in_end = live[i].in.end();
    set<int>::iterator out_it = live[i].out.begin();
    set<int>::iterator out_back = live[i].out.begin();
    set<int>::iterator out_end = live[i].out.end();
    while(in_it != in_end)
    {
      if(graph->find(*in_it) == graph->end())
      {
        GNode * temp = (GNode *) malloc(sizeof(GNode));
        temp->val = *in_it;
        temp->color = -1;
        temp->neighbors = new set<int>();
        (*graph)[*in_it] = temp;
      }
      while(in_back != in_it)
      {
        (*graph)[*in_back]->neighbors->insert(*in_it);
        (*graph)[*in_it]->neighbors->insert(*in_back);
        in_back++;
      }
      in_back = live[i].in.begin();
      in_it++;
    }

    while(out_it != out_end)
    {
      if(graph->find(*out_it) == graph->end())
      {
        GNode * temp = (GNode *) malloc(sizeof(GNode));
        temp->val = *out_it;
        temp->color = -1;
        temp->neighbors = new set<int>();
        (*graph)[*out_it] = temp;
      }
      while(out_back != out_it)
      {
        (*graph)[*out_back]->neighbors->insert(*out_it);
        (*graph)[*out_it]->neighbors->insert(*out_back);
        out_back++;
      }
      out_back = live[i].out.begin();
      out_it++;
    }
  
    
  }
  return graph;/*
  
  */

}

char itc(int i)
{
  return (char) (i + 96);
}

void printGraph(map<int, GNode*>* graph)
{
    map<int, GNode*>::iterator g_it = graph->begin();
    while(g_it != graph->end())
    {
      //printf("%c(%d)->", itc(g_it->second->val), g_it->second->color);
      printf("%d(%d)->", g_it->second->val, g_it->second->color);
      set<int>::iterator s_it = g_it->second->neighbors->begin();
      while(g_it->second->neighbors->end() != s_it)
      {
        //printf("%c ", itc(*s_it));
        printf("%d ", *s_it);
        s_it++;
      }
      printf("\n");
      g_it++;
    }
}

void freeGraph(map<int, GNode*> graph)
{
    map<int, GNode*>::iterator g_it = graph.begin();
    while(g_it != graph.end())
    {
      delete g_it->second->neighbors;
      free(g_it->second);
      g_it++;
    }
}

int max_color;
int max_no_color;
long long counter = 0;
int isLegal(GNode * node, int color, map<int, GNode*>* nodes)
{
  set<int>::iterator it = node->neighbors->begin();
  set<int>::iterator end = node->neighbors->end();
  while(it != end)
  {
    if((*nodes)[*it]->color == color)
    {
      return 0;
    } 
    it++;
  }
  return 1;
}

set<int> used_colors = set<int>();

map<int, GNode*> * best_config = new map<int, GNode*>();
map<int, int> * mapping;
vector<set<int> *>* connecteds = new vector<set<int>*>();
map<int, int> * set_map = new map<int, int>();
vector<GNode*>* sorted_graph;
int best_cost = -1;
int init = 1;
int check_subgraph = 0;

void copyGraph(map<int, GNode*>* orig)
{
  map<int, GNode*>::iterator it = orig->begin();
  map<int, GNode*>::iterator end = orig->end();

  while(it != end)
  {
    if(init == 1)
    {
      (*best_config)[it->first] = (GNode*) malloc(sizeof(GNode));
      (*best_config)[it->first]->neighbors = new set<int>(*(it->second->neighbors));
      
    }
    (*best_config)[it->first]->color = it->second->color;
    (*best_config)[it->first]->val = it->second->val;
    it++;
  }
  init = 0;
}


int isConnected(int val, int depth)
{
  for(int i = 0; i < depth; i++)
  {
    int prev_val = (*sorted_graph).at(i)->val;
    //printf("%d\n", (*set_map)[val]);
    //printf("%d\n", connecteds->size());
    set<int>* blah = connecteds->at((*set_map)[val]);
    if(blah->find(prev_val) != blah->end())
    {
      return 1;
    }
  }
  return 0;
}


void printUsed()
{
  set<int>::iterator it = used_colors.begin();
  set<int>::iterator end = used_colors.end();
  while(it != end)
  {
    printf("%d ", *it);
    it++;
  }
  printf("\n");
}

int j = 0;
int found_end = 0;

void reassess()
{
  int i = 0;
  used_colors.clear();
  for(i = 0; i < sorted_graph->size(); i++)
  {
    int col = sorted_graph->at(i)->color;
    
    //printf("Node: %d, Color: %d\n", sorted_graph->at(i)->val, sorted_graph->at(i)->color);
    if(col != -1)
    {
      used_colors.insert(sorted_graph->at(i)->color);
    }
  }
  printUsed(); 
}

int expand(STNode * node, int depth)
{
  if(counter % 10000 == 0)
  {
      time_t curr_time = time(NULL);
      if((long long) curr_time - (long long) start > 120)
      {
        printf("TOOK LONGER THAN 120 seconds. Current Best:\n");
        printGraph(best_config);
        long long diff = (long long) curr_time - (long long) start;
        printf("DIFF: %lld\n", diff);
        printf("COST: %d\n", best_cost);
        exit(0);
      }

  }
  if(depth+1 != max_color)
  {
    counter++;
    if(counter % 10000 == 0)
    {
      printf("%d\n", counter);
    }

        //printf("%d\n", counter);
  } 
  int val = sorted_graph->at(depth)->val;
  GNode * orig_node = (*(node->graph))[val];
    //printf("%lld\n", counter);
  map<int, GNode*>::iterator it = node->graph->begin();
  map<int, GNode*>::iterator end = node->graph->end();
  int color;
  //int best_cost = -1;
  /*int *best_assign = (int *) malloc(max_color * 4);  
  int i;
  for(i = 0; i < depth; i++)
  {
    best_assign[i] = it->second->color;
    it++;
  }*/

  advance(it, depth);
  int local_best = -1;
  //map<int, GNode*>* clone = cloneGraph(node->graph);
  
  STNode * child = (STNode *) malloc(sizeof(STNode));
  //child->graph = clone;
  child->graph = node->graph;

  map<int, GNode*>::iterator clone_it = child->graph->begin();
  advance(clone_it, depth);

  GNode * clone_node = (*(child->graph))[val];

  child->depth = depth+1;
  child->parent = node;
  child->child = 0;

  int added_new = 0;
  int fell_through = 0;
  int blah = 1;
  int delay_add = 0;
  /*if(check_subgraph == 1 && isConnected(val, depth-1) == 0)
  {
    delay_add = 1;
  }*/
  int found_legal = 0;
  for(color = 0; color < max_no_color; color++)
  {
    int erase = 0;
     if(added_new == 1)
        {
          clone_node->color = -1;
          //freeGraph(*child->graph);
          //delete child->graph;
          free(child);
          return best_cost;
          //continue;
        }
    //V3if(isLegal(it->second, color, node->graph))
    if(isLegal(orig_node, color, node->graph))
    {
      found_legal = 1;
      //--LOGGING--printf("LEGAL COLOR: %d found at depth: %d\n", color, depth);
      //--LOGGING--printUsed();
      //--LOGGING--printGraph(*node->graph);
      //--LOGGING--printf("%d\n", used_colors.size());
      if(used_colors.find(color) == used_colors.end())
      {
        if(added_new == 1)
        {
          clone_node->color = -1;
          //freeGraph(*child->graph);
          //delete child->graph;
          free(child);
          return best_cost;
          //continue;
        }

        added_new = 1;
        erase = 1;
        child->cost = node->cost + 1;
        used_colors.insert(color);
      } 
      else
      {
        child->cost = node->cost;
      }

      if(child->cost >= best_cost && best_cost != -1)
      {
        if(erase)
        {
          used_colors.erase(color);
        }
        continue;
      }

      child->rev_op_color = color;
      //V3clone_it->second->color = color;  
      clone_node->color = color;  
      if(depth+1 < max_color)
      {
        int current_best = expand(child, depth+1);
        if(found_end == 1)
        {
          if(depth != j - 1)
          {
            clone_node->color = -1;
            free(child);
            return best_cost;  
          }
          else
          {
            found_end = 0;
            printf("BACKTRACKED TO DEPTH: %d\n", depth);
            reassess();
          }
        }
        if(current_best < best_cost || best_cost == -1)
        {
          //local_best = current_best;
          best_cost = current_best;
          /*map<int, GNode*>::iterator temp = node->graph->begin();
          advance(temp, depth);
          int count = depth;
          while(temp != end)
          {
//printf("%d\n", count);
            best_assign[count] = temp->second->color;
            count++;
            temp++;
          }*/
        } 
      }
      else
      {
 
/*printf("Depth: %d\n", depth);
printf("Node: %d\n", it->second->val);
printf("Color: %d\n", color);
printf("***\n");
 */
       if(child->cost < best_cost || best_cost == -1)
        {
        counter++;
          if(counter % 10000 == 0)
            printf("%d\n", counter);

          //local_best = child->cost;
          best_cost = child->cost;
          copyGraph(child->graph);
          for(j = 0; j < max_color; j++)
          {
            if(sorted_graph->at(j)->color == (best_cost - 1))
            {
              printf("Cq = %d and is assigned to x%d\n", best_cost-1, j);
              found_end = 1;
              break;   

            }
          }
          //best_assign[depth] = color;
        }
      }
      //printf("It is legal to color node %c the color of %d\n", itc(it->second->val), color);  
                       
    }
    if(delay_add == 1 && found_legal == 1)
      added_new = 1;

    if(erase)
    {
      used_colors.erase(color);
    }
  }
  //while(it != end)
  //{
/*if(best_assign[i] == -1)
{
 printf("HERE!\n");
 printf("Depth: %d\n", depth);
printf("Node: %d\n", it->second->val);
printf("Color: %d\n", color);
printf("***\n");
  
}*/
 /*   it->second->color = best_assign[i];
    //printf("best_assign[i]: %d\n", best_assign[i]);
    it++;
    i++;
  }*/
  clone_node->color = -1;
  //freeGraph(*child->graph);
  //delete child->graph;
  free(child);
  
  //free(best_assign);
  //return local_best;
  return best_cost;
}

int maxDegree(map<int, GNode*>* graph)
{
  map<int, GNode*>::iterator it = graph->begin();
  map<int, GNode*>::iterator end = graph->end();
  int max = -1;
  while(it != end)
  {
    if(it->second->neighbors->size() > max || max == -1)
    {
      max = it->second->neighbors->size();
    }
    it++;
  }
  return max;
}


void run(map<int, GNode*>* graph)
{
  start = time(NULL);
  printf("START TIME: %lld\n", (long long) start);
  STNode * root = (STNode *) malloc(sizeof(STNode));
  root->graph = graph;
  root->cost = 0;
  root->rev_op_node = -1;
  root->rev_op_color = -1;
  root->depth = 0;
  root->parent = 0;
  root->child = 0;

  max_color = root->graph->size();
  max_no_color = maxDegree(graph) + 1;
  //printf("%d\n", max_color);

  expand(root, 0);
  free(root);
}

GNode* cloneNode(GNode * orig)
{
  GNode* ret = (GNode*) malloc(sizeof(GNode));
  ret->color = orig->color;
  ret->val = orig->val;
  ret->neighbors = new set<int>(*orig->neighbors);
  return ret;
}

map<int, GNode*>* cloneGraph(map<int, GNode*>* orig)
{
  map<int, GNode*>::iterator it = orig->begin();
  map<int, GNode*>::iterator end = orig->end();
  map<int, GNode*>* ret = new map<int, GNode*>();
  
  while(it != end)
  {
    (*ret)[it->first] = cloneNode(it->second);
    it++;
  }
  return ret;
}

map<int, GNode*> * createGraph(char* filename)
{
  FILE *fp = fopen(filename, "r");
  char line[1024];
  char num[5];
  num[4] = '\0';

  map<int, GNode*> * ret = new map<int,GNode*>();

  int node_num = -1;
  set<int> * neighbors; 
  while(fgets(line, 1024, fp) != NULL)
  {
    GNode * temp = (GNode*) malloc(sizeof(GNode));
    temp->neighbors = new set<int>();
    int len = strlen(line);
    int i = 0;
    int digit_count = 0;
    while(i < 1024)
    {
      if(line[i] == ' ')
      {
        num[digit_count] = '\0';
        node_num = atoi(num);
        i++;
        break;
      }
      else
      {
        num[digit_count] = line[i];
      }
      digit_count++;
      i++;
    }

    printf("%d: ", node_num);
    digit_count = 0;
    
    while(i < len) 
    {
      if(line[i] == ' ')
      {
        i++;
        break;
      }
      i++;
    }
    while(i < len)
    {
      if(line[i] == ' ')
      {
        num[digit_count] = '\0';
        temp->neighbors->insert(atoi(num));
        digit_count = -1;
        printf("%d ", atoi(num));
      }
      else
      {
        num[digit_count] = line[i];
      }
      digit_count++;
      i++;
    }
    num[digit_count] = '\0';
    if(strlen(num) != 0)
    {
      temp->neighbors->insert(atoi(num));
      printf("%d\n", atoi(num));
    }
    else
    {
      printf("\n");
    }
    edges += temp->neighbors->size();
    digit_count = -1;
    temp->val = node_num;
    temp->color = -1;
    (*ret)[node_num] = temp;
  }

  fclose(fp);
  return ret;
}

int getPosition(vector<GNode*>* graph, int degree)
{
  int size = graph->size();
  int i = 0;
  if(size == 0)
  {
    return 0;
  }
  while(i < size)
  {
    if(graph->at(i)->neighbors->size() < degree)
    {
      break;
    }
    i++;
  }
  return i;
}

void printSortedGraph(vector<GNode*> *graph)
{
  int index = 0;
  int size = graph->size();
  while(index < size)
  {
    printf("%d(%d)->", graph->at(index)->val, graph->at(index)->color);
    set<int>::iterator s_it = graph->at(index)->neighbors->begin();
    while(graph->at(index)->neighbors->end() != s_it)
    {
      //printf("%c ", itc(*s_it));
      printf("%d ", *s_it);
      s_it++;
    }
    printf("\n");
    index++;
  }
}

vector<GNode*>* sortGraph(map<int, GNode*> *graph)
{
  map<int, GNode*>::iterator g_it = graph->begin();
  vector<GNode*>* sorted = new vector<GNode*>();
  while(g_it != graph->end())
  {
    int pos = getPosition(sorted, g_it->second->neighbors->size());
    vector<GNode*>::iterator v_it = sorted->begin();
    advance(v_it, pos);
    
    sorted->insert(v_it, g_it->second); 
    g_it++;
  }
  return sorted;
}

map<int, int>* mappingFunc(vector<GNode*> *sorted, map<int, GNode*> *graph)
{
  map<int, int>* mapping = new map<int,int>();
  int size = sorted->size();
  for(int i = 0; i < size; i++)
  {
    int val = sorted->at(i)->val;
    (*mapping)[val] = i;
    printf("Node %d maps to position %d\n", val, i);
  }
  
  return mapping;
}

void constructReachability(map<int, GNode*>* graph)
{
  set<int> unused = set<int>();

  map<int, GNode*>::iterator g_it = graph->begin();
  map<int, GNode*>::iterator end = graph->end();

  while(g_it != end)
  {
    unused.insert(g_it->first);
    g_it++;
  }

  //vector<set<int> *> connecteds;
  //map<int, int> set_map;
  int index = 0;
  while(unused.size() != 0)
  {
    set<int>* connected = new set<int>();
    int val = *(unused.begin());
    set<int> working;
    working.insert(val);
    while(working.size() != 0)
    {
      int first = *(working.begin());
      set<int>* neighbors = (*graph)[first]->neighbors;
      (*set_map)[first] = index;
      connected->insert(first);
      unused.erase(first);
      working.erase(first);
      set<int> new_working;
      set_union(working.begin(), working.end(), neighbors->begin(), neighbors->end(), inserter(new_working, new_working.begin()));
      working.clear();
      set_intersection(new_working.begin(), new_working.end(), unused.begin(), unused.end(), inserter(working, working.begin()));
    }
    index++;
    connecteds->push_back(connected);
  }
  /*for(int i = 0; i < connecteds->size(); i++)
  {
    set<int> connected = *(connecteds->at(i));
    set<int>::iterator c_it = connected.begin();
    while(c_it != connected.end())
    {
      printf("%d ", *c_it);
      c_it++;
    }
    printf("\n");
  }*/

  /*map<int, int>::iterator m_it = set_map->begin();
  while(m_it != set_map->end())
  {
    printf("%d: %d\n", m_it->first, m_it->second);
    m_it++;
  }*/
  if(connecteds->size() > 1)
    check_subgraph = 1;
  printf("BLAH\n");
}


int main(int argc, char ** argv)
{
  Insn* insns;
  Liveness* live;
  map<int, GNode*>* graph;

  int lines = -1;
  if(strcmp(argv[1],"-g") == 0)
  {
    graph = createGraph(argv[2]);
  }
  else
  {
    lines = populate_instructions(argv[1], &insns);
    print_insns(insns, lines);
    live = produceLiveness(insns, lines);
    graph = generateNodes(live, lines);
  }

  /*int lines = populate_instructions(argv[1], &insns);
  print_insns(insns, lines);
  live = produceLiveness(insns, lines);

  graph = generateNodes(live, lines);
  */
  //vector<GNode*> * sorted_graph;
  //map<int, int> * mapping;
  printf("-------\n");
  printGraph(graph);
  printf("-------\n");
  sorted_graph = sortGraph(graph);
  mapping = mappingFunc(sorted_graph, graph);
  printSortedGraph(sorted_graph);
  printf("-------\n");
  constructReachability(graph);
  run(graph);

  printGraph(best_config);

  //map<int, GNode*>* clone = cloneGraph(&graph);
  //printGraph(*clone);
  printf("COUNT: %d\n", counter);
  printf("COLORS: %d\n", best_cost);
  printf("EDGES: %d\n", edges/2);
  if(strcmp(argv[1],"-g") == 1)
  {
    freeInsns(insns, lines);
    freeLiveness(live, lines);
  }
  freeGraph(*graph);
  delete graph;
  freeGraph(*best_config);
  delete best_config;
  delete mapping;
  delete sorted_graph;
  printf("%d\n", best_cost);
  printf("%lld\n", counter);
}
