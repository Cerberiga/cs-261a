#include <stdio.h>
#include <vector>
#include <stdlib.h>
#include <time.h>

using namespace std;

int a[20];
int size = 7;

void sort_func(vector<int>* src)
{
  for(int i = 0; i < size; i++)
  {
    int vec_size = src->size();
    int half = vec_size / 4;
    int pos = vec_size / 2;
    int start = 0;
    if(vec_size == 0)
    {
      pos = 0; 
    }
    else if(vec_size == 1)
    {
      if(a[i] > src->at(pos))
      {
        pos = 0;
      }
      else
      {
        pos = 1;
      }
    }
    else
    {
      while(vec_size >= 2)
      {
        if(vec_size%2 == 1)
        {
          if(a[i] <= src->at(start + vec_size - 1))
          {
            pos = start + vec_size;
            break;
          }
          else
          {
            vec_size = vec_size - 1;
            half = vec_size /4;
            pos = vec_size/2 + start;
          }
        }
        int diff = 0;
        int new_start = start;
        if(a[i] > src->at(pos))
        {
          diff = -half; 
          if(vec_size != 2 && (vec_size/2)%2 == 1)
          {
            diff = diff - 1;
          }
          if(vec_size == 2 && a[i] > src->at(pos-1))
          {
            diff = diff - 1; 
          }
        }
        else
        {
          diff = half;
          new_start = pos; 
          if(vec_size == 2)
          {
            diff += 1;
          }
        }

        vec_size = vec_size / 2;
        start = new_start;
        pos = pos + diff;
        half = half/2;          
      }
    }
    vector<int>::iterator it = src->begin();
    advance(it, pos); 
    src->insert(it, a[i]);

  for(int j = 0; j < src->size(); j++)
  {
    printf("%d ", src->at(j));
  }
  printf("\n");


  }
}

int main()
{

  /*for(int i =0; i < size; i++)
  {
    a[i] = rand()%10;
  }*/
  a[0] = 2;
  a[1] = 1;
  a[2] = 2;
  a[3] = 0;
  a[4] = 0;
  a[5] = 0;
  a[6] = 0;
  vector<int> blah = vector<int>();
  sort_func(&blah);
}
