#include <stdio.h>
#include <set>
#include <list>
#include <vector>
#include <map>
//#include <unordered_set>
using namespace std;

struct line{
  int use[3];
  int def;
  int alt_dest;
  vector<int> alt_preds;
};

struct liveness{
  set<int> in;
  set<int> out;
};

struct interval{
  int start;
  int end;
};

struct graph_node{
  int val;
  int color;
  set<int>* neighbors;
  //unordered_set<int>* illegal;
};

struct search_tree_node{
  map<int, struct graph_node*>* graph;
  int cost;
  int rev_op_node;
  int rev_op_color;
  int depth;
  struct search_tree_node* parent;
  struct search_tree_node* child;
};

