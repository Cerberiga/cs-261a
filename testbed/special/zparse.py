import os
import fileinput
import random
from subprocess import call
import sys

if __name__=='__main__':

  file_name = sys.argv[1]
  f = open(file_name)
  i = 0
  s1 = ""
  for line in f:
    if(i == 3):
      s1 = line
      break
    i = i + 1

  no_nodes = s1.split(" ")[2]
  no_edges = s1.split(" ")[3]
  j = 1
  arr = []
  while(j < int(no_nodes) + 1):
    arr.append(str(j) + " ->")
    j = j + 1 
  
  for line in f:
    source = line.split(" ")[1]
    dest = line.split(" ")[2].strip("\n")
    arr[int(source) - 1] = arr[int(source) - 1] + " " + dest
    arr[int(dest) - 1] = arr[int(dest) - 1] + " " + source
  for line in arr:
    print line
