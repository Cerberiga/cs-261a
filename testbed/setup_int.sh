#!/bin/sh


#python generate_int.py 200 50 1 0 
#python generate_int.py 200 50 2 0
#python generate_int.py 200 50 3 0
#python generate_int.py 200 50 4 0

python generate_int.py 200 50 0 0 > insns/200_50_0_0
python generate_int.py 200 50 0 1 > insns/200_50_0_1
python generate_int.py 200 50 0 2 > insns/200_50_0_2
python generate_int.py 200 50 0 3 > insns/200_50_0_3
python generate_int.py 200 50 0 4 > insns/200_50_0_4
python generate_int.py 200 50 0 5 > insns/200_50_0_5
python generate_int.py 200 50 0 6 > insns/200_50_0_6
python generate_int.py 200 50 0 7 > insns/200_50_0_7
python generate_int.py 200 50 0 8 > insns/200_50_0_8
python generate_int.py 200 50 0 9 > insns/200_50_0_9
python generate_int.py 200 50 0 10 > insns/200_50_0_10
python generate_int.py 200 50 0 11 > insns/200_50_0_11
python generate_int.py 200 50 0 12 > insns/200_50_0_12
python generate_int.py 200 50 0 13 > insns/200_50_0_13
python generate_int.py 200 50 0 14 > insns/200_50_0_14

python generate_int.py 200 50 1 0 > insns/200_50_1_0
python generate_int.py 200 50 1 1 > insns/200_50_1_1
python generate_int.py 200 50 1 2 > insns/200_50_1_2
python generate_int.py 200 50 1 3 > insns/200_50_1_3
python generate_int.py 200 50 1 4 > insns/200_50_1_4
python generate_int.py 200 50 1 5 > insns/200_50_1_5
python generate_int.py 200 50 1 6 > insns/200_50_1_6
python generate_int.py 200 50 1 7 > insns/200_50_1_7
python generate_int.py 200 50 1 8 > insns/200_50_1_8
python generate_int.py 200 50 1 9 > insns/200_50_1_9
python generate_int.py 200 50 1 10 > insns/200_50_1_10
python generate_int.py 200 50 1 11 > insns/200_50_1_11
python generate_int.py 200 50 1 12 > insns/200_50_1_12
python generate_int.py 200 50 1 13 > insns/200_50_1_13
python generate_int.py 200 50 1 14 > insns/200_50_1_14

python generate_int.py 200 50 2 0 > insns/200_50_2_0
python generate_int.py 200 50 2 1 > insns/200_50_2_1
python generate_int.py 200 50 2 2 > insns/200_50_2_2
python generate_int.py 200 50 2 3 > insns/200_50_2_3
python generate_int.py 200 50 2 4 > insns/200_50_2_4
python generate_int.py 200 50 2 5 > insns/200_50_2_5
python generate_int.py 200 50 2 6 > insns/200_50_2_6
python generate_int.py 200 50 2 7 > insns/200_50_2_7
python generate_int.py 200 50 2 8 > insns/200_50_2_8
python generate_int.py 200 50 2 9 > insns/200_50_2_9
python generate_int.py 200 50 2 10 > insns/200_50_2_10
python generate_int.py 200 50 2 11 > insns/200_50_2_11
python generate_int.py 200 50 2 12 > insns/200_50_2_12
python generate_int.py 200 50 2 13 > insns/200_50_2_13
python generate_int.py 200 50 2 14 > insns/200_50_2_14

python generate_int.py 200 50 3 0 > insns/200_50_3_0
python generate_int.py 200 50 3 1 > insns/200_50_3_1
python generate_int.py 200 50 3 2 > insns/200_50_3_2
python generate_int.py 200 50 3 3 > insns/200_50_3_3
python generate_int.py 200 50 3 4 > insns/200_50_3_4
python generate_int.py 200 50 3 5 > insns/200_50_3_5
python generate_int.py 200 50 3 6 > insns/200_50_3_6
python generate_int.py 200 50 3 7 > insns/200_50_3_7
python generate_int.py 200 50 3 8 > insns/200_50_3_8
python generate_int.py 200 50 3 9 > insns/200_50_3_9
python generate_int.py 200 50 3 10 > insns/200_50_3_10
python generate_int.py 200 50 3 11 > insns/200_50_3_11
python generate_int.py 200 50 3 12 > insns/200_50_3_12
python generate_int.py 200 50 3 13 > insns/200_50_3_13
python generate_int.py 200 50 3 14 > insns/200_50_3_14

python generate_int.py 200 50 4 0 > insns/200_50_4_0
python generate_int.py 200 50 4 1 > insns/200_50_4_1
python generate_int.py 200 50 4 2 > insns/200_50_4_2
python generate_int.py 200 50 4 3 > insns/200_50_4_3
python generate_int.py 200 50 4 4 > insns/200_50_4_4
python generate_int.py 200 50 4 5 > insns/200_50_4_5
python generate_int.py 200 50 4 6 > insns/200_50_4_6
python generate_int.py 200 50 4 7 > insns/200_50_4_7
python generate_int.py 200 50 4 8 > insns/200_50_4_8
python generate_int.py 200 50 4 9 > insns/200_50_4_9
python generate_int.py 200 50 4 10 > insns/200_50_4_10
python generate_int.py 200 50 4 11 > insns/200_50_4_11
python generate_int.py 200 50 4 12 > insns/200_50_4_12
python generate_int.py 200 50 4 13 > insns/200_50_4_13
python generate_int.py 200 50 4 14 > insns/200_50_4_14

python generate_int.py 200 50 5 0 > insns/200_50_5_0
python generate_int.py 200 50 5 1 > insns/200_50_5_1
python generate_int.py 200 50 5 2 > insns/200_50_5_2
python generate_int.py 200 50 5 3 > insns/200_50_5_3
python generate_int.py 200 50 5 4 > insns/200_50_5_4
python generate_int.py 200 50 5 5 > insns/200_50_5_5
python generate_int.py 200 50 5 6 > insns/200_50_5_6
python generate_int.py 200 50 5 7 > insns/200_50_5_7
python generate_int.py 200 50 5 8 > insns/200_50_5_8
python generate_int.py 200 50 5 9 > insns/200_50_5_9
python generate_int.py 200 50 5 10 > insns/200_50_5_10
python generate_int.py 200 50 5 11 > insns/200_50_5_11
python generate_int.py 200 50 5 12 > insns/200_50_5_12
python generate_int.py 200 50 5 13 > insns/200_50_5_13
python generate_int.py 200 50 5 14 > insns/200_50_5_14

