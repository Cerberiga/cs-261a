#!/bin/sh


python generate_rand_exact.py 40 1 1 > 40_1/40_1_1.g
python generate_rand_exact.py 40 1 2 > 40_1/40_1_2.g
python generate_rand_exact.py 40 1 3 > 40_1/40_1_3.g
python generate_rand_exact.py 40 1 4 > 40_1/40_1_4.g
python generate_rand_exact.py 40 1 5 > 40_1/40_1_5.g
python generate_rand_exact.py 40 1 6 > 40_1/40_1_6.g
python generate_rand_exact.py 40 1 7 > 40_1/40_1_7.g
python generate_rand_exact.py 40 1 8 > 40_1/40_1_8.g
python generate_rand_exact.py 40 1 9 > 40_1/40_1_9.g

python generate_rand_exact.py 40 3 0 > 40_3/40_3_0.g
python generate_rand_exact.py 40 3 1 > 40_3/40_3_1.g
python generate_rand_exact.py 40 3 2 > 40_3/40_3_2.g
python generate_rand_exact.py 40 3 3 > 40_3/40_3_3.g
python generate_rand_exact.py 40 3 4 > 40_3/40_3_4.g
python generate_rand_exact.py 40 3 5 > 40_3/40_3_5.g
python generate_rand_exact.py 40 3 6 > 40_3/40_3_6.g
python generate_rand_exact.py 40 3 7 > 40_3/40_3_7.g
python generate_rand_exact.py 40 3 8 > 40_3/40_3_8.g
python generate_rand_exact.py 40 3 9 > 40_3/40_3_9.g

python generate_rand_exact.py 40 5 0 > 40_5/40_5_0.g
python generate_rand_exact.py 40 5 1 > 40_5/40_5_1.g
python generate_rand_exact.py 40 5 2 > 40_5/40_5_2.g
python generate_rand_exact.py 40 5 3 > 40_5/40_5_3.g
python generate_rand_exact.py 40 5 4 > 40_5/40_5_4.g
python generate_rand_exact.py 40 5 5 > 40_5/40_5_5.g
python generate_rand_exact.py 40 5 6 > 40_5/40_5_6.g
python generate_rand_exact.py 40 5 7 > 40_5/40_5_7.g
python generate_rand_exact.py 40 5 8 > 40_5/40_5_8.g
python generate_rand_exact.py 40 5 9 > 40_5/40_5_9.g

python generate_rand_exact.py 40 7 0 > 40_7/40_7_0.g
python generate_rand_exact.py 40 7 1 > 40_7/40_7_1.g
python generate_rand_exact.py 40 7 2 > 40_7/40_7_2.g
python generate_rand_exact.py 40 7 3 > 40_7/40_7_3.g
python generate_rand_exact.py 40 7 4 > 40_7/40_7_4.g
python generate_rand_exact.py 40 7 5 > 40_7/40_7_5.g
python generate_rand_exact.py 40 7 6 > 40_7/40_7_6.g
python generate_rand_exact.py 40 7 7 > 40_7/40_7_7.g
python generate_rand_exact.py 40 7 8 > 40_7/40_7_8.g
python generate_rand_exact.py 40 7 9 > 40_7/40_7_9.g

python generate_rand_exact.py 40 9 0 > 40_9/40_9_0.g
python generate_rand_exact.py 40 9 1 > 40_9/40_9_1.g
python generate_rand_exact.py 40 9 2 > 40_9/40_9_2.g
python generate_rand_exact.py 40 9 3 > 40_9/40_9_3.g
python generate_rand_exact.py 40 9 4 > 40_9/40_9_4.g
python generate_rand_exact.py 40 9 5 > 40_9/40_9_5.g
python generate_rand_exact.py 40 9 6 > 40_9/40_9_6.g
python generate_rand_exact.py 40 9 7 > 40_9/40_9_7.g
python generate_rand_exact.py 40 9 8 > 40_9/40_9_8.g
python generate_rand_exact.py 40 9 9 > 40_9/40_9_9.g

python generate_rand_exact.py 50 1 0 > 50_1/50_1_0.g
python generate_rand_exact.py 50 1 1 > 50_1/50_1_1.g
python generate_rand_exact.py 50 1 2 > 50_1/50_1_2.g
python generate_rand_exact.py 50 1 3 > 50_1/50_1_3.g
python generate_rand_exact.py 50 1 4 > 50_1/50_1_4.g
python generate_rand_exact.py 50 1 5 > 50_1/50_1_5.g
python generate_rand_exact.py 50 1 6 > 50_1/50_1_6.g
python generate_rand_exact.py 50 1 7 > 50_1/50_1_7.g
python generate_rand_exact.py 50 1 8 > 50_1/50_1_8.g
python generate_rand_exact.py 50 1 9 > 50_1/50_1_9.g

python generate_rand_exact.py 50 3 0 > 50_3/50_3_0.g
python generate_rand_exact.py 50 3 1 > 50_3/50_3_1.g
python generate_rand_exact.py 50 3 2 > 50_3/50_3_2.g
python generate_rand_exact.py 50 3 3 > 50_3/50_3_3.g
python generate_rand_exact.py 50 3 4 > 50_3/50_3_4.g
python generate_rand_exact.py 50 3 5 > 50_3/50_3_5.g
python generate_rand_exact.py 50 3 6 > 50_3/50_3_6.g
python generate_rand_exact.py 50 3 7 > 50_3/50_3_7.g
python generate_rand_exact.py 50 3 8 > 50_3/50_3_8.g
python generate_rand_exact.py 50 3 9 > 50_3/50_3_9.g

python generate_rand_exact.py 50 5 0 > 50_5/50_5_0.g
python generate_rand_exact.py 50 5 1 > 50_5/50_5_1.g
python generate_rand_exact.py 50 5 2 > 50_5/50_5_2.g
python generate_rand_exact.py 50 5 3 > 50_5/50_5_3.g
python generate_rand_exact.py 50 5 4 > 50_5/50_5_4.g
python generate_rand_exact.py 50 5 5 > 50_5/50_5_5.g
python generate_rand_exact.py 50 5 6 > 50_5/50_5_6.g
python generate_rand_exact.py 50 5 7 > 50_5/50_5_7.g
python generate_rand_exact.py 50 5 8 > 50_5/50_5_8.g
python generate_rand_exact.py 50 5 9 > 50_5/50_5_9.g

python generate_rand_exact.py 50 7 0 > 50_7/50_7_0.g
python generate_rand_exact.py 50 7 1 > 50_7/50_7_1.g
python generate_rand_exact.py 50 7 2 > 50_7/50_7_2.g
python generate_rand_exact.py 50 7 3 > 50_7/50_7_3.g
python generate_rand_exact.py 50 7 4 > 50_7/50_7_4.g
python generate_rand_exact.py 50 7 5 > 50_7/50_7_5.g
python generate_rand_exact.py 50 7 6 > 50_7/50_7_6.g
python generate_rand_exact.py 50 7 7 > 50_7/50_7_7.g
python generate_rand_exact.py 50 7 8 > 50_7/50_7_8.g
python generate_rand_exact.py 50 7 9 > 50_7/50_7_9.g

python generate_rand_exact.py 50 9 0 > 50_9/50_9_0.g
python generate_rand_exact.py 50 9 1 > 50_9/50_9_1.g
python generate_rand_exact.py 50 9 2 > 50_9/50_9_2.g
python generate_rand_exact.py 50 9 3 > 50_9/50_9_3.g
python generate_rand_exact.py 50 9 4 > 50_9/50_9_4.g
python generate_rand_exact.py 50 9 5 > 50_9/50_9_5.g
python generate_rand_exact.py 50 9 6 > 50_9/50_9_6.g
python generate_rand_exact.py 50 9 7 > 50_9/50_9_7.g
python generate_rand_exact.py 50 9 8 > 50_9/50_9_8.g
python generate_rand_exact.py 50 9 9 > 50_9/50_9_9.g


python generate_rand.py 40 1000 0 > 40_9/40_1000x_0.g
python generate_rand.py 40 1000 1 > 40_9/40_1000x_1.g

python generate_rand.py 50 2000 0 > 50_9/50_2000x_0.g
python generate_rand.py 50 2000 1 > 50_9/50_2000x_1.g

