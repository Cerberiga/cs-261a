/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Define to 1 if you have the <ext2fs/ext2fs.h> header file. */
#define HAVE_EXT2FS_EXT2FS_H 1

/* Define to 1 if you have the <ext2fs/ext2_fs.h> header file. */
#define HAVE_EXT2FS_EXT2_FS_H 1

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define to 1 if you have the <memory.h> header file. */
#define HAVE_MEMORY_H 1

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Name of package */
#define PACKAGE "ext3grep"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "carlo@alinoe.com"

/* Define to the full name of this package. */
#define PACKAGE_NAME "ext3grep"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "ext3grep 0.7.0"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "ext3grep"

/* Define to the version of this package. */
#define PACKAGE_VERSION "0.7.0"

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Define when this project is checked out using svn. */
/* #undef USE_SVN */

/* Version number of package */
#define VERSION "0.7.0"

/* Number of bits in a file offset, on hosts where this is settable. */
/* #undef _FILE_OFFSET_BITS */

/* Define for large files, on AIX-style hosts. */
/* #undef _LARGE_FILES */
