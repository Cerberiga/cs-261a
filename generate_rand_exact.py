import os
import fileinput
import random
from subprocess import call


if __name__=='__main__':
  random.seed(0)
  nodes = 50

  existing = {}
  no_edges = 123
  actual = 0
  #no_edges = random.randint(0, 3*nodes);
  x = 0
  while x < no_edges:
    first = random.randint(0, nodes-1) + 1
    second = random.randint(0, nodes-1) + 1
    if first == second:
      x = x + 1
      continue

    if first in existing:
      temp = existing[first]
      temp_len = len(temp)
      temp.add(second)
      if(temp_len == len(temp)):
        x = x + 1
        continue
      else:
        actual = actual + 1
    else:
      existing[first] = set()
      temp = existing[first]
      temp.add(second)
      actual = actual + 1

    if second in existing:
      temp = existing[second]
      temp_len = len(temp)
      temp.add(first)
    else:
      existing[second] = set()
      temp = existing[second]
      temp.add(first)
    x = x + 1
  print(actual)

  while actual < no_edges:
    i = 1
    while i < nodes:
      if i in existing:
        temp = existing[i]
        j = 1
        while j < nodes:
          if j not in temp and j != i:
            temp.add(j)
            if j in existing:
              temp = existing[j]
              temp.add(i)
            else:
              existing[j] = set()
              temp = existing[j]
              temp.add(i)
            actual = actual + 1
            break            
          j = j + 1
      else:
        existing[i] = set()
        temp = existing[i]
        if(i == 1):
          j = 2
        else:
          j = 1
        temp.add(j)
        if j in existing:
          temp = existing[j]
          temp.add(i)
        else:
          existing[j] = set()
          temp = existing[j] 
          temp.add(i)
        actual = actual + 1
      if actual >= no_edges:
        break
      i = i + 1 
  print(actual)        
  for x in range(0,nodes):
    index = x + 1
    s = str(index) + " ->"
    if index in existing:
      for y in existing[index]:
        s += " " + str(y)
    print s

  #for x in range(0, nodes+1):
  #  s = '' + str(x) + ' ->'
  #  no_edges = random.randint(0, 25)
  #  for y in range(0, no_edges):
  #    neighbor = random.randint(1, nodes)
  #    if neighbor == x:
  #     neighbor = ((neighbor + 1)%nodes)+1
  #    s += " " + str(neighbor)
  #  print s 
